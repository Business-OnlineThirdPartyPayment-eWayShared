package Business::OnlineThirdPartyPayment::eWayShared;

use strict;
use Business::OnlineThirdPartyPayment 3;
use vars qw($VERSION @ISA $DEBUG);

@ISA = qw(Business::OnlineThirdPartyPayment);
$VERSION = '0.01';

$DEBUG = 0;

sub set_defaults {
  my $self = shift;

  $self->server('www.eway.com.au') unless $self->server;
  $self->port('443') unless $self->port;
  $self->path('/gateway/payment.asp') unless $self->path;
  $self->build_subs(qw(authorization order_number result_code error_message));
}

sub use_3dsecure {
  my $self = shift;
  $self->{'use_3dsecure'} = shift if ( @_ );
  $self->path('/gateway_3d/payment.asp') if $self->{'use_3dsecure'};
  return $self->{'use_3dsecure'};
}

sub reference {
  # Situation: We've been POSTed back from the gateway's web site.  The 
  # POST data is in the argument.  We need to set the state of the object
  # and then return the reference number.
  my ($self, $data) = @_;
  my $status  = $data->{'ewayTrxnStatus'};
  $self->order_number($data->{'ewayTrxnReference'});
  if ( lc($status) eq 'true' ) { 
    $self->is_success(1);
    $self->authorization($data->{'eWAYAuthCode'}); 
    # not spelled like this in the spec
  }
  else {
    $self->is_success(0);
    $self->result_code($data->{'eWAYresponsecode'});
    $self->error_message($data->{'eWAYresponseText'});
  }
  return $data->{'ewayTrxnNumber'}; 
}

sub submit {
  # One of two situations:
  # "authorization only": We haven't sent anything yet and are just 
  # creating a pending transaction locally.  Set popup_url and remap 
  # transaction fields into collectitems.
  # OR
  # "post authorization: We've already called reference() with the 
  # callback data.
  my($self) = @_;
  my %content = $self->content;

  my $action = lc($content{'action'});
  die 'Third Party Payment supports "Authorization Only" and '.
  '"Post Authorization" transactions'
  if !($action eq 'authorization only' || $action eq 'post authorization');
  die 'eWay only supports credit card transactions' 
  if $self->transaction_type() ne 'CC';

  my @required = qw( amount reference );
  $self->required_fields(@required);

  if ( $action eq 'authorization only' ) {
    $self->is_success(1);
    my $url =
    "https://". $self->server().
    ($self->port != 443 ? ':'. $self->port() : ''). $self->path();
    $self->popup_url( $url );

    my %fields = (
      'login'           => 'ewayCustomerID',
      'amount'          => 'ewayTotalAmount',
      'first_name'      => 'ewayCustomerFirstName',
      'last_name'       => 'ewayCustomerLastName',
      'email'           => 'ewayCustomerEmail',
      'address'         => 'ewayCustomerAddress',
      'zip'             => 'ewayCustomerPostcode',
      'description'     => 'ewayCustomerInvoiceDescription',
      'invoice_number'  => 'ewayCustomerInvoiceRef',
      'reference'       => 'ewayTrxnNumber',
      'callback_url'    => 'eWAYURL',
    );
    $self->required_fields(qw(login amount first_name last_name));
    $self->remap_fields(%fields);
    my %content = $self->content;
    $content{'ewayTotalAmount'} *= 100;
    $self->collectitems([ map { $_ => $content{$_} } values(%fields) ]);
    $self->is_success(1);
    return;
  }
  elsif ( $action eq 'post authorization' ) {
    return; # everything has been set up already
  }
}

1;
__END__

=head1 NAME

Business::OnlineThirdPartyPayment::eWayShared - eWay backend for Business::OnlineThirdPartyPayment

=head1 DESCRIPTION

For detailed information see L<Business::OnlineThirdPartyPayment>.

Call "$transaction->use_3dsecure(1)" (or pass "'use_3dsecure' => 1"
to I<new()>) to enable the 3D-Secure gateway.

=head1 NOTE

=head1 COMPATIBILITY

eWay Shared Payments and 3D-Secure API, as of December 2010.

=head1 AUTHOR

Mark Wells <mark@freeside.biz>

Based on Business::OnlineThirdPartyPayment::Interswitchng by 
Jeff Finucane <interswitchng@weasellips.com>

=head1 SEE ALSO

perl(1). L<Business::OnlineThirdPartyPayment>.

=cut

